@extends('layouts/master')

@section('content')
<h1>New people</h1>
    <form action="{{ url('people') }}" method="post">
        @csrf
        <? $faker = Faker\Factory::create() ?>
        <div class="form-group">
            <label for="my-input">Firstname</label>
        <input id="my-input" class="form-control" type="text" name="fname" placeholder="{{$faker->firstname}}">
        </div>
        <div class="form-group">
            <label for="my-input">Lastname</label>
        <input id="my-input" class="form-control" type="text" name="lname" placeholder="{{$faker->lastname}}">
        </div>
        <div class="form-group">
            <label for="my-input">Age</label>
        <input id="my-input" class="form-control" type="text" name="age" placeholder="{{$faker->numberBetween(15, 80)}}">
        </div>
        <button type="submit" class="btn btn-success">Save</button>
        
        @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </form>

@endsection
