@extends('layouts.master')
@section('title', 'List')
@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('content')
@if (Session::has('message'))
<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('message') }}
</div>
@endif
    <a href="/add" class="btn btn-success my-2">New People</a>

    <table class="table table-dark text-center">
        <thead>
            <th>ID</th>
            <th>Fname</th>
            <th>Lname</th>
            <th>Age</th>
            <th>Created_at</th>
            <th>Updated_at</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach ($people as $p)
            <tr>
            <td>{{ $p->id }}</td>
            <td>{{ $p->fname }}</td>
            <td>{{ $p->lname }}</td>
            <td>{{ $p->age }}</td>
            <td>{{ $p->created_at }}</td>
            <td>{{ $p->updated_at }}</td>
            <td class="form-inline justify-content-center">
                <a class="btn btn-primary" href="{{ url('people/' . $p->id . '/edit') }}">Edit</a>
            <form class="ml-2" action="{{ url('people', [$p->id]) }}" method="POST">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </td>
            
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('js')
    <script>
    
    </script>
@endsection