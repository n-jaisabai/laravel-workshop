<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index($id) {
        $sum = $id + 4;
        $arr = [
            'id' => 2,
            'firstname' => 'Nattapong',
            'lastname' => 'Jaisabai',
            'skills' => [
                'thai' => 100,
                'eng' => 50
            ]
        ];

        // dd($arr); dump and die
        // die();

        return view('list')->with('obj', $sum);
    }
}
